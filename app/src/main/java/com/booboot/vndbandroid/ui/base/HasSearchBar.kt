package com.booboot.vndbandroid.ui.base

import android.view.View

interface HasSearchBar {
    fun searchBar(): View?
}