package com.booboot.vndbandroid.ui.base

interface HasTabs {
    fun currentFragment(): BaseFragment<*>?
}